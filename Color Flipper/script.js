
function getRandomColour(){
    
    //Generate randome number 1-255
    const r = Math.floor(Math.random()*255);
    const g = Math.floor(Math.random()*255);
    const b = Math.floor(Math.random()*255);

    //convert decimal in to hexa decimal number 
    const hexR = r.toString(16).padStart(2,0);
    const hexG = g.toString(16).padStart(2,0);
    const hexB = b.toString(16).padStart(2,0);

    //get random number in hex code
    const randomColor = "#".concat(hexR,hexG,hexB);
    console.log(randomColor);
    return randomColor;
}



document.getElementById('colorChangeButton').addEventListener('click' ,function() {

    const randomColor = getRandomColour();
    document.body.style.backgroundColor = randomColor;

}
);

const randomColor = getRandomColour();
document.body.style.backgroundColor = randomColor;

